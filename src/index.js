import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

// styles
import './styles/index.css';

// components
import Root from './components/app'
import Person from './components/pages/person'

// define routes
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
    ]
  },
  {
    path: "persons",
    element: <Person />,
  },
]);

// return main component
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);