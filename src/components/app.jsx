// index.js
import React from 'react'
// improt components
import Header from './layout/header'
import Banner from './layout/banner'

function Root() {
    return (
        <div className='app-container'>    
            <Header></Header>
            <main className="main">
                <Banner></Banner>
            </main>
        </div>
    );
}

export default Root;
