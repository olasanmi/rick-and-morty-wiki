import React from "react";
import Header from '../layout/header'
import Banner from '../layout/banner'

function Person() {

    // return components
    return (
        <>
            <Header ></Header>
            <main className="main">
                <Banner></Banner>
            </main>
        </>
    );
}

export default Person